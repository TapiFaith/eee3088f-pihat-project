# EEE3088F PiHat Project

The microHAT is a motor driver with position feedback HAT. It will be able to regulate speed of a motor, do proximity sensing of ferromagnetic materials and can be used in current detection. 
You can contribute to this project by adding the circuits of the different subsystems you are working on. I am working on the Status LED subsystem.
